<?xml version="1.0"?>
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><title>testmxlookup</title><link rel="stylesheet" type="text/css" href="style.css"/><meta name="generator" content="DocBook XSL Stylesheets Vsnapshot"/><link rel="home" href="#testmxlookup" title="testmxlookup"/><link xmlns="" rel="stylesheet" type="text/css" href="manpage.css"/><meta xmlns="" name="MSSmartTagsPreventParsing" content="TRUE"/><link xmlns="" rel="icon" href="icon.gif" type="image/gif"/><!--

Copyright 1998 - 2009 Double Precision, Inc.  See COPYING for distribution
information.

--></head><body><div class="refentry"><a id="testmxlookup" shape="rect"> </a><div class="titlepage"/><div class="refnamediv"><h2>Name</h2><p>testmxlookup — Look up mail servers for a domain</p></div><div class="refsynopsisdiv"><h2>Synopsis</h2><div class="cmdsynopsis"><p><code class="command">testmxlookup</code>  [ @<em class="replaceable"><code>ip-address</code></em>  |   --dnssec  |   --udpsize <em class="replaceable"><code>n</code></em>  |   --sts  |   --sts-override=<em class="replaceable"><code>mode</code></em>  |   --sts-purge ] {<em class="replaceable"><code>domain</code></em>}</p></div><div class="cmdsynopsis"><p><code class="command">testmxlookup</code>  { --sts-expire  |   --sts-cache-disable  |   --sts-cache-enable  |   --sts-cache-enable=<em class="replaceable"><code>size</code></em> }</p></div></div><div class="refsect1"><a id="testmxlookup_description" shape="rect"> </a><h2>DESCRIPTION</h2><p>
      <span class="command"><strong>testmxlookup</strong></span> reports the names and IP addresses of mail
      servers that receive mail for the <em class="replaceable"><code>domain</code></em>,
      as well as the <em class="replaceable"><code>domain</code></em>'s published
      <acronym class="acronym">STS</acronym> policy.
      This is useful in diagnosing mail delivery problems.
    </p><p>
      <span class="command"><strong>testmxlookup</strong></span> sends a DNS MX query for the specified
      domain, followed by A/AAAA queries, if needed.
      <span class="command"><strong>testmxlookup</strong></span> lists the
      hostname and the IP address of every mail server, and its MX priority.
      The domain's strict transport security
      (<acronym class="acronym">STS</acronym>) policy status, if one is published,
      precedes the mail server list.
    </p><div class="refsect2"><a id="testmxlookup_diagnostics" shape="rect"> </a><h3>DIAGNOSTICS</h3><p>
	The error message <span class="quote">“<span class="quote">Hard error</span>”</span> indicates that the
	domain does not exist,
	or does not have any mail servers. The error message "Soft error"
	indicates a temporary error condition (usually a network failure of
	some sorts, or the local DNS server is down).
      </p><p>
	<span class="quote">“<span class="quote">STS: testing</span>”</span> or
	<span class="quote">“<span class="quote">STS: enforcing</span>”</span> preceding the list of mail servers
	indicates that the domain publishes an <acronym class="acronym">STS</acronym>
	policy.
	<span class="quote">“<span class="quote">ERROR: STS Policy verification failed</span>”</span> appearing
	after an individual mail server
	indicates that the mail server's name does not meet the domain's
	<acronym class="acronym">STS</acronym> policy.
      </p><p>
	<span class="quote">“<span class="quote">STS: testing</span>”</span> or
	<span class="quote">“<span class="quote">STS: enforcing</span>”</span> by itself, with no further messages,
	indicates that all listed mail servers comply with the listed
	<acronym class="acronym">STS</acronym> policy. If you are attempting to install
	your own STS policy this is a simple means of checking its
	validity.
      </p></div><div class="refsect2"><a id="testmxlookup_options" shape="rect"> </a><h3>OPTIONS</h3><div class="variablelist"><dl class="variablelist"><dt><span class="term"><code class="literal">@ip-address</code></span></dt><dd><p>
	      Specify the DNS server's IP address, where to send the DNS
	      query to, overriding the default DNS server addresses read from
	      <code class="filename">/etc/resolv.conf</code>.
	    </p><p>
	      <span class="quote">“<span class="quote">ip-address</span>”</span> must be a literal, numeric,
	      IP address.
	    </p></dd><dt><span class="term"><code class="literal">--dnssec</code></span></dt><dd><p>
	      Enable the <code class="literal">DNSSEC</code> extension. If the DNS
	      server has <code class="literal">DNSSEC</code> enabled, and the
	      specified domain's DNS records are signed, the list of
	      IP addresses is suffixed by <span class="quote">“<span class="quote">(DNSSEC)</span>”</span>, indicating
	      a signed response.
	    </p><p>
	      This is a diagnostic option. Older DNS servers may respond with
	      an error, to a DNSSEC query.
	    </p></dd><dt><span class="term"><code class="literal">--udpsize</code> <em class="replaceable"><code>n</code></em></span></dt><dd><p>
	      Specify that <em class="replaceable"><code>n</code></em> is the largest
	      <acronym class="acronym">UDP</acronym> packet size that the DNS server may
	      send. This option is only valid together with
	      <span class="quote">“<span class="quote">--dnssec</span>”</span>.
	      If <span class="quote">“<span class="quote">--dnssec</span>”</span> always returns an error, try
	      <span class="quote">“<span class="quote">--udpsize 512</span>”</span> (the default setting is 1280
	      bytes, which is adequate for Ethernet, but other kinds of
	      networks may impose lower limits).
	    </p></dd><dt><span class="term"><code class="literal">--sts</code></span></dt><dd><p>
	      Do not issue an MX query, and display the domain's raw
	      <acronym class="acronym">STS</acronym> policy file.
	    </p></dd><dt><span class="term"><code class="literal">--sts-cache-disable</code></span></dt><dd><p>
	      Turn off <acronym class="acronym">STS</acronym> lookups, checking, and
	      verification. <acronym class="acronym">STS</acronym> is enabled by default,
	      but requires that a global systemwide list of
	      SSL certificate authorities is available, and
	      that <code class="envar">TLS_TRUSTCERTS</code> is specified in
	      @sysconfdir@/courierd. <acronym class="acronym">STS</acronym> can be disabled,
	      if needed.
	    </p></dd><dt><span class="term"><code class="literal">--sts-cache-enable</code></span></dt><dd><p>
	      Reenable <acronym class="acronym">STS</acronym> lookups, checking, and
	      verification, and set the size of the internal cache to its
	      default value. Specify <span class="quote">“<span class="quote"><code class="literal">=size</code></span>”</span>
	      to enable and set a non-default cache size, a positive value
	      indicating the approximate number of most recent domains
	      whose <acronym class="acronym">STS</acronym> policies get cached internally.
	    </p></dd><dt><span class="term"><code class="literal">--sts-override=<em class="replaceable"><code>policy</code></em></code></span></dt><dd><p>
	      Override the domain's
	      <acronym class="acronym">STS</acronym> enforcement mode.
	      <em class="replaceable"><code>policy</code></em> is one of:
	      <span class="quote">“<span class="quote">none</span>”</span>,
	      <span class="quote">“<span class="quote">testing</span>”</span>, or
	      <span class="quote">“<span class="quote">enforce</span>”</span>, and overrides the cached
	      domain <acronym class="acronym">STS</acronym> policy setting.
	    </p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>
		This is a diagnostic or a testing tool.
		<span class="application">Courier</span> may eventually purge
		the cached policy setting, or the domain can update its
		policy, replacing the overridden setting.
	      </p></div></dd><dt><span class="term"><code class="literal">--sts-purge</code></span></dt><dd><p>
	      Remove the domain's cached <acronym class="acronym">STS</acronym> policy,
	      and retrieve and cache the domain's policy, again.
	    </p></dd><dt><span class="term"><code class="literal">--sts-expire</code></span></dt><dd><p>
	      Execute
	      <span class="application">Courier</span>'s
	      <acronym class="acronym">STS</acronym> policy expiration process. Nothing
	      happens unless
	      <code class="filename">@localstatedir@/sts</code>'s size exceeds the
	      configured cache size setting.
	      The oldest cached policy files get removed
	      to bring the cache size down to its maximum size.
	    </p></dd></dl></div></div><div class="refsect2"><a id="testmxlookup_strict_transport_security" shape="rect"> </a><h3>STRICT TRANSPORT SECURITY</h3><p>
	<span class="application">Courier</span>
	automatically downloads and caches domains'
	<acronym class="acronym">STS</acronym> policy files by default, in an internal
	cache with a default size of 1000 domains.
      </p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>
	  The cache size setting is approximate.
	  <span class="application">Courier</span>
	  purges stale cache entries periodically, and the size of the
	  cache can temporarily exceed its set size, by as much as a factor
	  of two.
	  <code class="filename">@localstatedir@/sts</code>
	  must be owned by @mailuser@:@mailgroup@, and uses one file
	  per mail domain. The maximum cache size depends on the
	  capabilities of the underlying filesystem.
	</p><p>
	  <span class="command"><strong>testmxlookup</strong></span> must be executed with sufficient
	  privileges to access the cache directory (by root, or by
	  @mailuser@). Without sufficient privileges
	  <span class="command"><strong>testmxlookup</strong></span> still attempts to use
	  the cache directory
	  even without write permissions on it, as long as it's
	  accessible, and attempts to download the STS policy for a domain
	  that's not already cached; but, of course, won't be able to
	  save the downloaded policy in the cache directory.
	</p></div></div></div><div class="refsect1"><a id="testmxlookup_see_also" shape="rect"> </a><h2>SEE ALSO</h2><p>
<a class="ulink" href="courier.html" target="_top" shape="rect"><span class="citerefentry"><span class="refentrytitle">courier</span>(8)</span></a>,
<a class="ulink" href="https://www.ietf.org/rfc/rfc1035.txt" target="_top" shape="rect">RFC 1035</a>,
<a class="ulink" href="https://www.ietf.org/rfc/rfc8461.txt" target="_top" shape="rect">RFC 8461</a>.
    </p></div></div></body></html>
